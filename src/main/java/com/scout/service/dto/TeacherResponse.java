package com.scout.service.dto;

public class TeacherResponse {
	
	private String teacherId;

	private String teacherEmail;

	private String teacherPhoneNo;

	private String teacherName;

	private String teacherAddress;

	private String teacherPassword;

	public String getTeacherId() {
		return teacherId;
	}

	public String getTeacherEmail() {
		return teacherEmail;
	}

	public String getTeacherPhoneNo() {
		return teacherPhoneNo;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public String getTeacherAddress() {
		return teacherAddress;
	}

	public String getTeacherPassword() {
		return teacherPassword;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	public void setTeacherPhoneNo(String teacherPhoneNo) {
		this.teacherPhoneNo = teacherPhoneNo;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public void setTeacherAddress(String teacherAddress) {
		this.teacherAddress = teacherAddress;
	}

	public void setTeacherPassword(String teacherPassword) {
		this.teacherPassword = teacherPassword;
	}
}
