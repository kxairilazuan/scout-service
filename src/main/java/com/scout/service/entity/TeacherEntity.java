package com.scout.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "system_teacher")
public class TeacherEntity {
	
	@Id
	@Column(name = "teacher_id")
	private String teacherId;

	@Column(name = "teacher_email")
	private String teacherEmail;

	@Column(name = "teacher_phone_no")
	private String teacherPhoneNo;

	@Column(name = "teacher_name")
	private String teacherName;

	@Column(name = "teacher_address")
	private String teacherAddress;

	@Column(name = "teacher_password")
	private String teacherPassword;

	public String getTeacherId() {
		return teacherId;
	}

	public String getTeacherEmail() {
		return teacherEmail;
	}

	public String getTeacherPhoneNo() {
		return teacherPhoneNo;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public String getTeacherAddress() {
		return teacherAddress;
	}

	public String getTeacherPassword() {
		return teacherPassword;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	public void setTeacherPhoneNo(String teacherPhoneNo) {
		this.teacherPhoneNo = teacherPhoneNo;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public void setTeacherAddress(String teacherAddress) {
		this.teacherAddress = teacherAddress;
	}

	public void setTeacherPassword(String teacherPassword) {
		this.teacherPassword = teacherPassword;
	}
}
