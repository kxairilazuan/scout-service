package com.scout.service.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.scout.service.utility.BaseUtility;
import com.scout.service.dto.Response;
import com.scout.service.dto.TeacherRequest;
import com.scout.service.dto.TeacherResponse;
import com.scout.service.service.TeacherService;

@RestController
@RequestMapping("/scout")
public class ScoutController {
	
	@Autowired
	TeacherService teacherService;
	
	@GetMapping("/welcome")
	public String welcome() {
		return "welcome to scout service";
	}
	
	@GetMapping("/teachers")
	public ResponseEntity<Response> getTeachers() {
		HttpStatus http_status = HttpStatus.OK;
		String error_desc = null;
		Boolean message_status = false;
		String message_desc = null;
		String message_code = null;
		String message_dev = null;
		Map<Object, Object> object_map = new HashMap<Object, Object>();
		
		List<TeacherResponse> teacherResponses = teacherService.getTeachers();
		
		if (BaseUtility.isListNull(teacherResponses)) {
			message_status = false;
			message_desc = "NULL";
		} else {
			message_status = true;
			object_map.put("teachers", teacherResponses);
		}
		
		return returnResponse(null, http_status, error_desc, message_status, message_desc, message_code, message_dev, object_map);
	}
	
	@GetMapping("/teacher/{teacherId}")
	public ResponseEntity<Response> getTeacher(@PathVariable("teacherId") String teacherId) {
		HttpStatus http_status = HttpStatus.OK;
		String error_desc = null;
		Boolean message_status = false;
		String message_desc = null;
		String message_code = null;
		String message_dev = null;
		Map<Object, Object> object_map = new HashMap<Object, Object>();
		
		if (BaseUtility.isNotBlank(teacherId)) {
			TeacherResponse teacherResponse = teacherService.getTeacher(teacherId);
			
			if (BaseUtility.isObjectNotNull(teacherResponse)) {
				message_status = true;
				message_desc = "SUCCESS";
				object_map.put("teacher", teacherResponse);
			} else {
				error_desc = "FAIL";
			}
		} else {
			http_status = HttpStatus.BAD_REQUEST;
			error_desc = "FAIL";
		}
		
		return returnResponse(null, http_status, error_desc, message_status, message_desc, message_code, message_dev, object_map);
	}
	
	@PostMapping("/teacher")
	public ResponseEntity<Response> insertTeacher(@RequestBody TeacherRequest teacherRequest) {
		HttpStatus http_status = HttpStatus.OK;
		String error_desc = null;
		Boolean message_status = false;
		String message_desc = null;
		String message_code = null;
		String message_dev = null;
		Map<Object, Object> object_map = new HashMap<Object, Object>();
		
		if (BaseUtility.isObjectNotNull(teacherRequest)) {
			TeacherResponse teacherResponse = teacherService.insertTeacher(teacherRequest);
			
			if (BaseUtility.isObjectNotNull(teacherResponse)) {
				message_status = true;
				message_desc = "SUCCESS";
				object_map.put("teacher", teacherResponse);
			} else {
				error_desc = "FAIL";
			}
		} else {
			http_status = HttpStatus.BAD_REQUEST;
			error_desc = "FAIL";
		}
		
		return returnResponse(null, http_status, error_desc, message_status, message_desc, message_code, message_dev, object_map);
	}
	
	@PutMapping("/teacher")
	public ResponseEntity<Response> updateTeacher(@RequestBody TeacherRequest teacherRequest) {
		HttpStatus http_status = HttpStatus.OK;
		String error_desc = null;
		Boolean message_status = false;
		String message_desc = null;
		String message_code = null;
		String message_dev = null;
		Map<Object, Object> object_map = new HashMap<Object, Object>();
		
		if (BaseUtility.isObjectNotNull(teacherRequest)) {
			TeacherResponse teacherResponse = teacherService.updateTeacher(teacherRequest);
			
			if (BaseUtility.isObjectNotNull(teacherResponse)) {
				message_status = true;
				message_desc = "SUCCESS";
				object_map.put("teacher", teacherResponse);
			} else {
				error_desc = "FAIL";
			}
		} else {
			http_status = HttpStatus.BAD_REQUEST;
			error_desc = "FAIL";
		}
		
		return returnResponse(null, http_status, error_desc, message_status, message_desc, message_code, message_dev, object_map);
	}
	
	@DeleteMapping("/teacher/{teacherId}")
	public ResponseEntity<Response> deleteTeacher(@PathVariable("teacherId") String teacherId) {
		HttpStatus http_status = HttpStatus.OK;
		String error_desc = null;
		Boolean message_status = false;
		String message_desc = null;
		String message_code = null;
		String message_dev = null;
		Map<Object, Object> object_map = new HashMap<Object, Object>();
		
		if (BaseUtility.isNotBlank(teacherId)) {
			Boolean deleteStatus = teacherService.deleteTeacher(teacherId);
			
			if (deleteStatus) {
				message_status = true;
				message_desc = "SUCCESS";
			} else {
				error_desc = "FAIL";
			}
		} else {
			http_status = HttpStatus.BAD_REQUEST;
			error_desc = "FAIL";
		}
		
		return returnResponse(null, http_status, error_desc, message_status, message_desc, message_code, message_dev, object_map);
	}
	
	public ResponseEntity<Response> returnResponse(Errors errors, HttpStatus http_status, String error_desc, Boolean msg_status, String msg_desc, String msg_code, String msg_dev, Map<Object, Object> object_map) {
		if (BaseUtility.isObjectNotNull(errors)) {
			if (errors.hasErrors()) {
				for (ObjectError objectError: errors.getAllErrors()) {
					error_desc = error_desc + objectError.getDefaultMessage();
					
					if (errors.getErrorCount() > 1) {
						error_desc = error_desc + ", ";
					}
				}
			}
		}
		
		Response response = new Response();
		response.setTime_stamp(LocalDateTime.now());
		response.setStatus_code(http_status.value());
		response.setStatus_desc(http_status);
		response.setError_desc(error_desc);
		response.setMessage_status(msg_status);
		response.setMessage_desc(msg_desc);
		response.setMessage_code(msg_code);
		response.setMessage_dev(msg_dev);
		response.setData(object_map);
		
		return ResponseEntity.ok(response);
	}
}
