package com.scout.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scout.service.entity.TeacherEntity;

@Repository
public interface TeacherRepository extends JpaRepository<TeacherEntity, String> {

	TeacherEntity findByTeacherId(String teacherId);
	
	Integer deleteByTeacherId(String teacherId);
}
