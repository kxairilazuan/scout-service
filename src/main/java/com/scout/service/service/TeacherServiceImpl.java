package com.scout.service.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scout.service.dao.TeacherRepository;
import com.scout.service.dto.TeacherRequest;
import com.scout.service.dto.TeacherResponse;
import com.scout.service.entity.TeacherEntity;
import com.scout.service.utility.BaseUtility;

import jakarta.transaction.Transactional;

@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	TeacherRepository teacherRepository;
	
	@Override
	public List<TeacherResponse> getTeachers() {
		List<TeacherResponse> teacherResponses = new ArrayList<>();
		List<TeacherEntity> teacherEntities = teacherRepository.findAll();
		
		for (TeacherEntity teacherEntity : teacherEntities) {
			TeacherResponse teacherResponse = new TeacherResponse();
			
			teacherResponse.setTeacherId(teacherEntity.getTeacherId());
			teacherResponse.setTeacherEmail(teacherEntity.getTeacherEmail());
			teacherResponse.setTeacherPhoneNo(teacherEntity.getTeacherPhoneNo());
			teacherResponse.setTeacherName(teacherEntity.getTeacherName());
			teacherResponse.setTeacherAddress(teacherEntity.getTeacherAddress());
			teacherResponse.setTeacherPassword(teacherEntity.getTeacherPassword());
			
			teacherResponses.add(teacherResponse);
		}
		
		return teacherResponses;
	}

	@Override
	public TeacherResponse getTeacher(String teacherId) {
		TeacherResponse teacherResponse = new TeacherResponse();
		TeacherEntity teacherEntity = teacherRepository.findByTeacherId(teacherId);
		
		if (BaseUtility.isObjectNotNull(teacherEntity)) {
			teacherResponse.setTeacherId(teacherEntity.getTeacherId());
			teacherResponse.setTeacherEmail(teacherEntity.getTeacherEmail());
			teacherResponse.setTeacherPhoneNo(teacherEntity.getTeacherPhoneNo());
			teacherResponse.setTeacherName(teacherEntity.getTeacherName());
			teacherResponse.setTeacherAddress(teacherEntity.getTeacherAddress());
			teacherResponse.setTeacherPassword(teacherEntity.getTeacherPassword());
		}
		
		return teacherResponse;
	}

	@Override
	public TeacherResponse insertTeacher(TeacherRequest teacherRequest) {
		TeacherResponse teacherResponse = new TeacherResponse();
		TeacherEntity newTeacherEntity = new TeacherEntity();
		
		newTeacherEntity.setTeacherId(BaseUtility.generateId());
		newTeacherEntity.setTeacherEmail(teacherRequest.getTeacherEmail());
		newTeacherEntity.setTeacherPhoneNo(teacherRequest.getTeacherPhoneNo());
		newTeacherEntity.setTeacherName(teacherRequest.getTeacherName());
		newTeacherEntity.setTeacherAddress(teacherRequest.getTeacherAddress());
		newTeacherEntity.setTeacherPassword(teacherRequest.getTeacherPassword());
		
		TeacherEntity insertedTeacherEntity = teacherRepository.save(newTeacherEntity);
		
		if (BaseUtility.isObjectNotNull(insertedTeacherEntity)) {
			teacherResponse.setTeacherId(insertedTeacherEntity.getTeacherId());
			teacherResponse.setTeacherEmail(insertedTeacherEntity.getTeacherEmail());
			teacherResponse.setTeacherPhoneNo(insertedTeacherEntity.getTeacherPhoneNo());
			teacherResponse.setTeacherName(insertedTeacherEntity.getTeacherName());
			teacherResponse.setTeacherAddress(insertedTeacherEntity.getTeacherAddress());
			teacherResponse.setTeacherPassword(insertedTeacherEntity.getTeacherPassword());
		}
		
		return teacherResponse;
	}

	@Override
	public TeacherResponse updateTeacher(TeacherRequest teacherRequest) {
		TeacherResponse teacherResponse = new TeacherResponse();
		TeacherEntity existedTeacherEntity = teacherRepository.findByTeacherId(teacherRequest.getTeacherId());
		
		existedTeacherEntity.setTeacherEmail(teacherRequest.getTeacherEmail());
		existedTeacherEntity.setTeacherPhoneNo(teacherRequest.getTeacherPhoneNo());
		existedTeacherEntity.setTeacherName(teacherRequest.getTeacherName());
		existedTeacherEntity.setTeacherAddress(teacherRequest.getTeacherAddress());
		existedTeacherEntity.setTeacherPassword(teacherRequest.getTeacherPassword());
		
		TeacherEntity insertedTeacherEntity = teacherRepository.save(existedTeacherEntity);
		
		if (BaseUtility.isObjectNotNull(insertedTeacherEntity)) {
			teacherResponse.setTeacherId(insertedTeacherEntity.getTeacherId());
			teacherResponse.setTeacherEmail(insertedTeacherEntity.getTeacherEmail());
			teacherResponse.setTeacherPhoneNo(insertedTeacherEntity.getTeacherPhoneNo());
			teacherResponse.setTeacherName(insertedTeacherEntity.getTeacherName());
			teacherResponse.setTeacherAddress(insertedTeacherEntity.getTeacherAddress());
			teacherResponse.setTeacherPassword(insertedTeacherEntity.getTeacherPassword());
		}
		
		return teacherResponse;
	}

	@Transactional
	public Boolean deleteTeacher(String teacherId) {
		Integer totalDeleted = 0;
		
		try {
			totalDeleted = teacherRepository.deleteByTeacherId(teacherId);
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
		
		if (totalDeleted > 0) {
			return true;
		}
		
		return false;
	}

}
