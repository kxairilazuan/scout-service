package com.scout.service.service;

import java.util.List;

import com.scout.service.dto.TeacherRequest;
import com.scout.service.dto.TeacherResponse;

public interface TeacherService {

	List<TeacherResponse> getTeachers();
	
	TeacherResponse getTeacher(String teacherId);
	
	TeacherResponse insertTeacher(TeacherRequest teacherRequest);
	
	TeacherResponse updateTeacher(TeacherRequest teacherRequest);
	
	Boolean deleteTeacher(String teacherId);
}
